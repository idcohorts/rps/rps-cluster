# Ansible Collection - rps.cluster

Documentation for the collection.

`ansible-playbook -i infra/inventory/ rps.cluster.hetzner_robot_servers_info`


## Development

### local dev-env

You need to have lxd installed and initialized locally on your machine

deploy local cluster dev-env:

`ansible-playbook -i dev-env/ dev-env.yaml`
